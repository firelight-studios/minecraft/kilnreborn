package com.gitlab.firelight.kilnreborn.client.screen;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.block.screen.KilnScreenHandler;
import com.gitlab.firelight.kilnreborn.client.recipebook.KilnBookRecipeScreen;
import net.minecraft.client.gui.screens.inventory.AbstractFurnaceScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class KilnScreen extends AbstractFurnaceScreen<KilnScreenHandler> {
    private static final ResourceLocation TEXTURE = new ResourceLocation(KilnReborn.MOD_ID, "textures/gui/container/kiln.png");

    public KilnScreen(KilnScreenHandler abstractFurnaceMenu, Inventory inventory, Component component) {
        super(abstractFurnaceMenu, new KilnBookRecipeScreen(), inventory, component, TEXTURE);
    }
}
