package com.gitlab.firelight.kilnreborn;

import com.gitlab.firelight.kilnreborn.registry.*;
import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KilnReborn implements ModInitializer {
    public static final String MOD_ID = "kilnreborn";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    @Override
    public void onInitialize() {
        LOGGER.info("Initializing mod");

        KilnBlockEntityInit.loadBlockEntities();
        KilnBlockInit.loadBlocks();
        KilnItemInit.registerItems();
        KilnRecipeInit.loadRecipes();
        KilnScreenHandlerInit.loadScreens();
        KilnStatInit.registerStats();
    }
}
