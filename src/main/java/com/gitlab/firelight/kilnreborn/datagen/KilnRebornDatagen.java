package com.gitlab.firelight.kilnreborn.datagen;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.compat.ModBlockCompat;
import com.gitlab.firelight.kilnreborn.mixin.SimpleCookingRecipeBuilderAccessor;
import com.gitlab.firelight.kilnreborn.registry.KilnBlockInit;
import com.gitlab.firelight.kilnreborn.registry.KilnItemInit;
import com.gitlab.firelight.kilnreborn.registry.KilnRecipeInit;
import net.fabricmc.fabric.api.datagen.v1.DataGeneratorEntrypoint;
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricBlockLootTableProvider;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricRecipeProvider;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricTagProvider;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.CookingBookCategory;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.CopyNameFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSet;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class KilnRebornDatagen implements DataGeneratorEntrypoint {
    private static void registerFakeItem(String modId, String ...ids) {
        for (String id : ids) {
            Registry.register(BuiltInRegistries.ITEM, new ResourceLocation(modId, id), new Item(new Item.Properties()));
        }
    }

    @Override
    public void onInitializeDataGenerator(FabricDataGenerator dataGenerator) {
        for (String blockId : ModBlockCompat.naturalProgressionCobbledBlocks) {
            var id = new ResourceLocation("natprog", "cobbled_" + blockId);
            var block = Registry.register(BuiltInRegistries.BLOCK, id, new Block(BlockBehaviour.Properties.copy(Blocks.STONE)));
            Registry.register(BuiltInRegistries.ITEM, id, new BlockItem(block, new Item.Properties()));
        }

        registerFakeItem("thirst", "clay_bowl", "terracotta_bowl");

        var pack = dataGenerator.createPack();

        pack.addProvider(KilnLootTableProvider::new);
        pack.addProvider(KilnRecipeProvider::new);
        pack.addProvider(KilnTagProvider::new);
    }

    private static class KilnLootTableProvider extends FabricBlockLootTableProvider {
        public KilnLootTableProvider(FabricDataOutput output) {
            super(output);
        }

        @Override
        public void generate() {
            this.add(KilnBlockInit.KILN, LootTable.lootTable()
                    .setParamSet(
                        LootContextParamSet.builder()
                            .required(LootContextParams.BLOCK_ENTITY)
                            .build()
                    )
                .withPool(
                    this.applyExplosionCondition(KilnItemInit.KILN, LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(1.0F))
                        .add(
                            LootItem.lootTableItem(KilnItemInit.KILN).apply(
                                CopyNameFunction.copyName(CopyNameFunction.NameSource.BLOCK_ENTITY)
                            )
                        )
                    )
                )
            );
        }
    }

    private static class KilnRecipeProvider extends FabricRecipeProvider {
        public KilnRecipeProvider(FabricDataOutput output) {
            super(output);
        }

        private static ResourceLocation recipe(String name) {
            return new ResourceLocation(KilnReborn.MOD_ID, name + "_from_firing");
        }

        private static void registerCompatRecipe(Consumer<FinishedRecipe> exporter, String modId, String from, String to) {
            var fromItem = BuiltInRegistries.ITEM.get(new ResourceLocation(modId, from));
            var toItem = BuiltInRegistries.ITEM.get(new ResourceLocation(modId, to));

            firing(Ingredient.of(fromItem), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, toItem, 0.1f, COOKING_TIME)
                .unlockedBy("has_" + from, has(fromItem))
                .save(exporter, recipe(to));
        }

        public static final int COOKING_TIME = 100;

        @Override
        public void buildRecipes(Consumer<FinishedRecipe> exporter) {
            for (DyeColor color : DyeColor.values()) {
                // Terracotta
                var terracotta = BuiltInRegistries.ITEM.get(new ResourceLocation(color.getName() + "_terracotta"));
                var glazedTerracotta = BuiltInRegistries.ITEM.get(new ResourceLocation(color.getName() + "_glazed_terracotta"));

                firing(Ingredient.of(terracotta), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, glazedTerracotta, 0.1f, COOKING_TIME)
                    .unlockedBy("has_terracotta", has(terracotta))
                    .save(exporter, recipe(color.getName() + "_glazed_terracotta"));
            }

            firing(Ingredient.of(Items.CLAY), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, Items.TERRACOTTA, 1f, COOKING_TIME)
                .unlockedBy("has_clay", has(Items.CLAY))
                .save(exporter, recipe("terracotta"));

            firing(Ingredient.of(Items.CACTUS), CookingBookCategory.MISC, RecipeCategory.DECORATIONS, Items.GREEN_DYE, 1f, COOKING_TIME)
                .unlockedBy("has_cactus", has(Items.CACTUS))
                .save(exporter, recipe("green_dye"));

            firing(Ingredient.of(Items.SEA_PICKLE), CookingBookCategory.MISC, RecipeCategory.DECORATIONS, Items.LIME_DYE, 1f, COOKING_TIME)
                .unlockedBy("has_sea_pickle", has(Items.SEA_PICKLE))
                .save(exporter, recipe("lime_dye"));

            firing(Ingredient.of(Items.SAND), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, Items.GLASS, 0.1f, COOKING_TIME)
                .unlockedBy("has_sand", has(Items.SAND))
                .save(exporter, recipe("glass"));

            firing(Ingredient.of(Items.NETHERRACK), CookingBookCategory.MISC, RecipeCategory.BUILDING_BLOCKS, Items.NETHER_BRICK, 0.1f, COOKING_TIME)
                .unlockedBy("has_netherrack", has(Items.NETHERRACK))
                .save(exporter, recipe("nether_brick"));

            firing(Ingredient.of(Items.BASALT), CookingBookCategory.BLOCKS, RecipeCategory.DECORATIONS, Items.SMOOTH_BASALT, 0.1f, COOKING_TIME)
                .unlockedBy("has_basalt", has(Items.BASALT))
                .save(exporter, recipe("smooth_basalt"));

            var crackedVariants = new String[] { "polished_blackstone_bricks", "deepslate_bricks", "deepslate_tiles", "nether_bricks", "stone_bricks" };
            for (String s : crackedVariants) {
                // Cracked blocks
                var regular = BuiltInRegistries.ITEM.get(new ResourceLocation(s));
                var cracked = BuiltInRegistries.ITEM.get(new ResourceLocation("cracked_" + s));

                firing(Ingredient.of(regular), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, cracked, 0.1f, COOKING_TIME)
                    .unlockedBy("has_regular", has(regular))
                    .save(exporter, recipe("cracked_" + s));
            }

            firing(Ingredient.of(Items.QUARTZ_BLOCK), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, Items.SMOOTH_QUARTZ, 0.1f, COOKING_TIME)
                .unlockedBy("has_quartz_block", has(Items.QUARTZ_BLOCK))
                .save(exporter, recipe("smooth_quartz"));

            var smoothVariants = new String[] { "red_sandstone", "sandstone", "stone" };
            for (String s : smoothVariants) {
                // Cracked blocks
                var regular = BuiltInRegistries.ITEM.get(new ResourceLocation(s));
                var smooth = BuiltInRegistries.ITEM.get(new ResourceLocation("smooth_" + s));

                firing(Ingredient.of(regular), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, smooth, 0.1f, COOKING_TIME)
                    .unlockedBy("has_regular", has(regular))
                    .save(exporter, recipe("smooth_" + s));
            }

            firing(Ingredient.of(ItemTags.LOGS_THAT_BURN), CookingBookCategory.MISC, RecipeCategory.MISC, Items.CHARCOAL, 0.15f, COOKING_TIME)
                .unlockedBy("has_log", has(ItemTags.LOGS_THAT_BURN))
                .save(exporter, recipe("charcoal"));

            firing(Ingredient.of(Items.CLAY_BALL), CookingBookCategory.MISC, RecipeCategory.DECORATIONS, Items.BRICK, 0.3f, COOKING_TIME)
                .unlockedBy("has_clay_ball", has(Items.CLAY_BALL))
                .save(exporter, recipe("brick"));

            firing(Ingredient.of(Items.COBBLED_DEEPSLATE), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, Items.DEEPSLATE, 0.1f, COOKING_TIME)
                .unlockedBy("has_cobbled_deepslate", has(Items.COBBLED_DEEPSLATE))
                .save(exporter, recipe("deepslate"));

            firing(Ingredient.of(Items.CHORUS_FRUIT), CookingBookCategory.MISC, RecipeCategory.DECORATIONS, Items.POPPED_CHORUS_FRUIT, 0.1f, COOKING_TIME)
                .unlockedBy("has_chorus", has(Items.CHORUS_FRUIT))
                .save(exporter, recipe("popped_chorus_fruit"));

            firing(Ingredient.of(Items.WET_SPONGE), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, Items.SPONGE, 0.15f, COOKING_TIME)
                .unlockedBy("has_wet_sponge", has(Items.WET_SPONGE))
                .save(exporter, recipe("sponge"));

            firing(Ingredient.of(Items.COBBLESTONE), CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, Items.STONE, 0.1f, COOKING_TIME)
                .unlockedBy("has_cobble", has(Items.COBBLESTONE))
                .save(exporter, recipe("stone"));

            // Crafting
            ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, KilnItemInit.KILN)
                .define('#', Items.BRICK)
                .pattern("###")
                .pattern("# #")
                .pattern("###")
                .unlockedBy("has_bricks", has(Items.BRICK))
                .save(exporter, new ResourceLocation(KilnReborn.MOD_ID, "kiln"));


            // Natural Progression compat
            {
                for (String blockId : ModBlockCompat.naturalProgressionCobbledBlocks) {
                    var cobbled = BuiltInRegistries.ITEM.get(new ResourceLocation("natprog", "cobbled_" + blockId));

                    firing(Ingredient.of(cobbled),
                        CookingBookCategory.BLOCKS, RecipeCategory.BUILDING_BLOCKS, BuiltInRegistries.ITEM.get(new ResourceLocation("minecraft", blockId)), 0.1f, COOKING_TIME)
                        .unlockedBy("has_cobbled_" + blockId, has(cobbled))
                        .save(exporter, recipe("natprog_cobbled_" + blockId + "_to_" + blockId)
                    );
                }
            }

            // Thirst compat
            registerCompatRecipe(exporter, "thirst", "clay_bowl", "terracotta_bowl");
        }

        public static SimpleCookingRecipeBuilder firing(Ingredient ingredient, CookingBookCategory cookingCategory, RecipeCategory recipeCategory, ItemLike itemLike, float experience, int cookingTime) {
            return SimpleCookingRecipeBuilderAccessor.createSimpleCookingRecipeBuilder(recipeCategory, cookingCategory, itemLike, ingredient, experience, cookingTime, KilnRecipeInit.FIRING_SERIALIZER);
        }
    }

    private static class KilnTagProvider extends FabricTagProvider.BlockTagProvider {

        public KilnTagProvider(FabricDataOutput output, CompletableFuture<HolderLookup.Provider> registriesFuture) {
            super(output, registriesFuture);
        }

        @Override
        protected void addTags(HolderLookup.Provider arg) {
            getOrCreateTagBuilder(BlockTags.MINEABLE_WITH_PICKAXE)
                .add(KilnBlockInit.KILN);
        }
    }
}
