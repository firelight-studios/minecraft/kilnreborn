package com.gitlab.firelight.kilnreborn.registry;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.stats.Stats;

public class KilnStatInit {
    public static final ResourceLocation INTERACT_WITH_KILN = new ResourceLocation(KilnReborn.MOD_ID, "interact_with_kiln");

    public static void registerStats() {
        Registry.register(BuiltInRegistries.CUSTOM_STAT, INTERACT_WITH_KILN, INTERACT_WITH_KILN);
        Stats.CUSTOM.get(INTERACT_WITH_KILN);
    }
}
