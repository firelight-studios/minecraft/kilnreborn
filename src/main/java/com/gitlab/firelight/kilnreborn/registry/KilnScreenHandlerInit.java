package com.gitlab.firelight.kilnreborn.registry;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.block.screen.KilnScreenHandler;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.MenuType;

public class KilnScreenHandlerInit {
    public static final MenuType<KilnScreenHandler> KILN_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(new ResourceLocation(KilnReborn.MOD_ID, "kiln"), KilnScreenHandler::new);

    public static void loadScreens() {
    }
}
