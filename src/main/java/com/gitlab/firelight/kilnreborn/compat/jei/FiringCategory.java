package com.gitlab.firelight.kilnreborn.compat.jei;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import com.gitlab.firelight.kilnreborn.recipe.FiringRecipe;
import com.gitlab.firelight.kilnreborn.registry.KilnBlockInit;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.library.plugins.vanilla.cooking.AbstractCookingCategory;

public class FiringCategory extends AbstractCookingCategory<FiringRecipe> {
    public static final RecipeType<FiringRecipe> FIRING = RecipeType.create(KilnReborn.MOD_ID, "firing", FiringRecipe.class);

    public FiringCategory(IJeiHelpers helpers) {
        super(helpers.getGuiHelper(), KilnBlockInit.KILN, "category.kilnreborn.firing", 100);
    }

    @Override
    public RecipeType<FiringRecipe> getRecipeType() {
        return FIRING;
    }
}
