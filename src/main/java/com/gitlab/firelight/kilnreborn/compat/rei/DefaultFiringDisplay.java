package com.gitlab.firelight.kilnreborn.compat.rei;

import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.entry.EntryIngredient;
import me.shedaniel.rei.plugin.common.displays.cooking.DefaultCookingDisplay;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;

import java.util.List;

public class DefaultFiringDisplay extends DefaultCookingDisplay {
    public DefaultFiringDisplay(AbstractCookingRecipe recipe) {
        super(recipe);
    }

    public DefaultFiringDisplay(List<EntryIngredient> input, List<EntryIngredient> output, CompoundTag tag) {
        super(input, output, tag);
    }

    @Override
    public CategoryIdentifier<?> getCategoryIdentifier() {
        return KilnREIClientPlugin.FIRING;
    }
}
