package com.gitlab.firelight.kilnreborn.compat.jei;

import com.gitlab.firelight.kilnreborn.KilnReborn;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import net.minecraft.resources.ResourceLocation;

@JeiPlugin
public class KilnJEIClientPlugin implements IModPlugin {
    @Override
    public ResourceLocation getPluginUid() {
        return new ResourceLocation(KilnReborn.MOD_ID, "jei_plugin");
    }

    @Override
    public void registerCategories(IRecipeCategoryRegistration registration) {
        registration.addRecipeCategories(new FiringCategory(registration.getJeiHelpers()));
    }
}
